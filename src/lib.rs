pub mod kdemeta {
    use glob::Pattern;
    use serde::Deserialize;
    use serde_yaml::Value;
    use std::collections::BTreeMap as Map;
    use std::path::Path;
    use std::path::PathBuf;
    use walkdir::WalkDir;

    use crate::kdemeta;

    #[derive(Deserialize)]
    pub struct KdeCi {
        #[serde(rename = "Dependencies")]
        pub dependencies: Option<Vec<Dependencies>>,
        #[serde(rename = "RuntimeDependencies")]
        pub runtime_dependencies: Option<Vec<Dependencies>>,
    }

    #[derive(Deserialize)]
    pub struct MetaData {
        pub repopath: String,
        pub projectpath: String,
        pub repoactive: bool,
    }

    #[derive(Deserialize)]
    pub struct Dependencies {
        pub on: Vec<String>,
        pub require: Require,
    }

    #[derive(Deserialize)]
    pub struct Require {
        #[serde(flatten)]
        pub other: Map<String, Value>,
    }

    #[derive(Deserialize)]
    pub struct BranchGroups {
        #[serde(rename = "kf5-qt5")]
        pub kf5_qt5: Option<String>,
        #[serde(rename = "kf6-qt6")]
        pub kf6_qt6: Option<String>,
        #[serde(rename = "stable-kf5-qt5")]
        pub stable_kf5_qt5: Option<String>,
        #[serde(rename = "stable-kf6-qt6")]
        pub stable_kf6_qt6: Option<String>,
    }

    pub struct LogicalModuleStructure {
        pub entries: Map<String, BranchGroups>,
    }

    pub struct RepoMetadata {
        path: PathBuf,
        pub projects: Vec<MetaData>,
    }

    impl RepoMetadata {
        pub fn new(path: &Path) -> RepoMetadata {
            let ignored_categories = vec![
                "websites",
                "wikitolearn",
                "webapps",
                "historical",
                "documentation",
                "sysadmin",
                "neon",
                "packaging",
                "unmaintained",
            ];

            let entries: Vec<MetaData> = WalkDir::new(path.join(Path::new("projects-invent")))
                .into_iter()
                .filter_map(|e| e.ok())
                .filter(|name| name.file_name().to_string_lossy().ends_with(".yaml"))
                .filter(|entry| {
                    !ignored_categories
                        .contains(&entry.path().iter().nth_back(2).unwrap().to_str().unwrap())
                })
                .map(|entry| {
                    let md: kdemeta::MetaData =
                        serde_yaml::from_reader(std::fs::File::open(entry.path()).unwrap())
                            .unwrap();
                    md
                })
                .collect();

            RepoMetadata {
                path: path.to_path_buf(),
                projects: entries,
            }
        }

        pub fn logical_module_structure(&self) -> Map<String, BranchGroups> {
            let file = std::fs::File::open(
                self.path
                    .join(Path::new("dependencies/logical-module-structure.json")),
            );

            let json: serde_json::Value = serde_json::from_reader(file.unwrap()).unwrap();

            let groups = json["groups"].as_object().unwrap();

            let weighted_keys = get_project_keys(groups);

            let mut result: Map<String, BranchGroups> = Default::default();

            for project in &self.projects {
                for key in &weighted_keys {
                    if Pattern::new(&key.glob)
                        .unwrap()
                        .matches(&project.projectpath)
                    {
                        let b: BranchGroups =
                            serde_json::from_value(groups[&key.glob].to_owned()).unwrap();

                        result.insert(project.projectpath.to_string(), b);

                        break;
                    }
                }
            }

            return result;
        }
    }

    struct ProjectKey {
        glob: String,
        weight: usize,
    }

    fn get_project_keys(groups: &serde_json::Map<String, serde_json::Value>) -> Vec<ProjectKey> {
        // We need the most specific glob to win, e.g. kde/workspace/* must win over kde/*,
        // and kde/workspace/foo must win over kde/workspace/*
        let mut weighted_keys: Vec<ProjectKey> = groups
            .keys()
            .into_iter()
            .map(|key| {
                let weight;

                if !key.contains("*") {
                    // Exact match is highest weight
                    weight = 10;
                } else {
                    // Prefer more specific matches
                    weight = key.matches("/").count();
                }

                return ProjectKey {
                    glob: key.to_string(),
                    weight,
                };
            })
            .collect();

        weighted_keys.sort_by(|a, b| a.weight.cmp(&b.weight));
        weighted_keys.reverse();

        return weighted_keys;
    }
}

#[cfg(test)]
mod tests {
    use crate::kdemeta;
    use std::path::Path;

    #[test]
    fn exploration() {
        let rmd =
            kdemeta::RepoMetadata::new(Path::new("/home/nico/kde/src/sysadmin-repo-metadata"));

        let lms = rmd.logical_module_structure();

        println!("{:?}", lms.get("kde/workspace/kwin").unwrap().kf6_qt6);
    }
}
